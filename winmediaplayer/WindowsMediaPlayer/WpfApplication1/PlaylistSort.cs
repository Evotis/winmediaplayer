﻿using System.Windows;
using System.Linq;

namespace WindowsMediaPlayer
{
    public partial class MainWindow
    {
        public void SortByArtist(object sender, RoutedEventArgs e)
        {
            int index = 0;
            var numQuery = from data in QueueList
                           orderby data.Artist
                           select data;

            foreach (Playlist data in numQuery)
            {
                QueueList[index] = null;
                QueueList[index] = data;
                index += 1;
            }
            _display = Display.Artist;
        }

        public void SortByGenre(object sender, RoutedEventArgs e)
        {
            int index = 0;
            var numQuery = from data in QueueList
                           orderby idGenre[data.Genre]
                           select data;

            foreach (Playlist data in numQuery)
            {
                QueueList[index] = null;
                QueueList[index] = data;
                index += 1;
            }
            _display = Display.Genre;
        }

        public void SortByAlbum(object sender, RoutedEventArgs e)
        {
            int index = 0;
            var numQuery = from data in QueueList
                           orderby data.Album
                           select data;

            foreach (Playlist data in numQuery)
            {
                QueueList[index] = null;
                QueueList[index] = data;
                index += 1;
            }
            _display = Display.Album;
        }

    }
}
