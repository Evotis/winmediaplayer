﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using MyToolkit.Multimedia;
using Application = System.Windows.Application;
using DataFormats = System.Windows.Forms.DataFormats;
using DragEventArgs = System.Windows.DragEventArgs;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;
using MessageBox = System.Windows.MessageBox;
using MouseEventArgs = System.Windows.Input.MouseEventArgs;
using Orientation = System.Windows.Controls.Orientation;
using Timer = System.Timers.Timer;

namespace WindowsMediaPlayer
{
    public partial class MainWindow
    {
        private Timer timer;
        int doubleClick;
        bool fullScreen;
        int DoubleCount = 1;
        private int CollapseCount;
        Window Playlist;
        StackPanel _stackPanel;
        int PlayIndex;
        bool isHidden = false;
        private readonly string[] extention = { ".mp3", ".wmv", ".avi", ".mp4", ".jpg", ".gif", ".bmp", ".png", "" };

        public MainWindow()
        {
            InitializeComponent();
            Loaded += MainWindow_Loaded;
        }

        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            QueueList = new ObservableCollection<Playlist>();
            Playlist = new Window();
            _stackPanel = new StackPanel { Orientation = Orientation.Vertical };
            idGenre = new Dictionary<string, string>();

            initIdGenre();
            Playlist.Closing += try_closing;
            Playlist.PreviewKeyDown += Playlist_OnPreviewKeyDown;

            Box = initializeListBox();
            BArtist = initializeBArtist();
            BAlbum = initializeBAlbum();
            BGenre = initializeBGenre();

            try
            {
                LoadPlaylist();
            }
            catch (Exception)
            {
                MessageBox.Show("Playlist corrupted.");
            }
            Box.ItemsSource = QueueList;
            Box.MouseDoubleClick += Double_Click;
            Playlist.Height = 500;
            Playlist.Width = 500;
            Playlist.MinHeight = 500;
            Playlist.MinWidth = 500;
            Playlist.MaxHeight = 500;
            Playlist.MaxWidth = 500;
            _stackPanel.Children.Add(Box);
            _stackPanel.Children.Add(BArtist);
            _stackPanel.Children.Add(BAlbum);
            _stackPanel.Children.Add(BGenre);
            Playlist.Content = _stackPanel;
        }

        private void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Dispatcher.Invoke(() =>
            {
                if (!isChanging && Video.NaturalDuration.HasTimeSpan)
                    Position.Text = Video.Position.ToString(@"hh\:mm\:ss");
                else if (!Video.NaturalDuration.HasTimeSpan)
                    Position.Text = "00:00:00";
                if (Video.NaturalDuration.HasTimeSpan && timeElapsed.Maximum == 10)
                    timeElapsed.Maximum = Video.NaturalDuration.TimeSpan.TotalSeconds;
                if (isPlaying && !isChanging)
                {
                    timeElapsed.Value = Video.Position.TotalSeconds;
                    oldValue = timeElapsed.Value;
                }
                if (DoubleCount % 10 == 0)
                    doubleClick = 0;
                if (Video.NaturalDuration.HasTimeSpan && Video.Position.TotalSeconds == Video.NaturalDuration.TimeSpan.TotalSeconds)
                    ChangeVideo();
                if (CollapseCount % 5 == 0)
                    CollapseAll();
                DoubleCount += 1;
                CollapseCount += 1;
            });
        }

        void Video_MediaOpened(object sender, RoutedEventArgs e)
        {
            if (Video.NaturalDuration.HasTimeSpan)
                timeElapsed.Maximum = Video.NaturalDuration.TimeSpan.TotalSeconds;
        }

        private void Video_Drop(object sender, DragEventArgs e)
        {
            var FileName = (String[])e.Data.GetData(DataFormats.FileDrop, true);

            if (Array.IndexOf(extention, Path.GetExtension(FileName[0])) == -1)
            {
                MessageBox.Show("Cannot display this file format");
                return;
            }
            if (FileName.Length > 0)
            {
                var videoPath = FileName[0];
                Video.Source = new Uri(videoPath);
                Video.Play();
                Play.Content = "Pause";
                timeElapsed.Minimum = 0;
                timeElapsed.Value = 0;
                if (timer != null)
                    timer.Stop();
                timer = new Timer { Interval = TimeSpan.FromSeconds(1).TotalMilliseconds };
                timer.Elapsed += timer_Elapsed;
                timer.Start();
            }
            e.Handled = true;
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            SavePlaylist();
            Application.Current.Shutdown();
        }

        private void CollapseAll()
        {
            if (!isPlaying)
                Play.Visibility = Visibility.Collapsed;
            else
                Pause.Visibility = Visibility.Collapsed;
            Next.Visibility = Visibility.Collapsed;
            Previous.Visibility = Visibility.Collapsed;
            Position.Visibility = Visibility.Collapsed;
            timeElapsed.Visibility = Visibility.Collapsed;
            Volume.Visibility = Visibility.Collapsed;
            open.Visibility = Visibility.Collapsed;
            OpenPlaylist.Visibility = Visibility.Collapsed;
            Plist.Visibility = Visibility.Collapsed;
            isHidden = true;
        }

        private void Video_OnMouseMove(object sender, MouseEventArgs e)
        {
            if (!isPlaying)
                Play.Visibility = Visibility.Visible;
            else
                Pause.Visibility = Visibility.Visible;
            Next.Visibility = Visibility.Visible;
            Previous.Visibility = Visibility.Visible;
            Position.Visibility = Visibility.Visible;
            timeElapsed.Visibility = Visibility.Visible;
            Volume.Visibility = Visibility.Visible;
            open.Visibility = Visibility.Visible;
            OpenPlaylist.Visibility = Visibility.Visible;
            Plist.Visibility = Visibility.Visible;
            CollapseCount = 1;
            isHidden = false;
        }

        private void MainWindow_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F11)
                FullScreen(sender, null);
            if (e.Key == Key.O)
                open_Click(sender, null);
            if (e.Key == Key.S)
                list_Click(sender, null);
            if (e.Key == Key.P)
                add_playlist(sender, null);
            if (e.Key == Key.B)
                play_video(sender, null);
        }
    }
}
