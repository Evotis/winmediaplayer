﻿using System;
using System.Windows;
using System.Windows.Input;

namespace WindowsMediaPlayer
{
    public partial class MainWindow
    {
        double oldValue;
        private TimeSpan time;
        private bool isChanging;

        private void changeVolume(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Video.Volume = e.NewValue;
        }

        private void timeElapsed_changed(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if ((timeElapsed.Value - oldValue) > 5 || (timeElapsed.Value - oldValue) < -1)
            {
                time = new TimeSpan(0, 0, 0, (int)timeElapsed.Value, 0);
                Position.Text = time.ToString(@"hh\:mm\:ss");
                isChanging = true;
                oldValue = timeElapsed.Value;
            }
        }

        private void TimeElapsed_OnPreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            isChanging = false;
            Video.Position = time;
        }
    }
}
