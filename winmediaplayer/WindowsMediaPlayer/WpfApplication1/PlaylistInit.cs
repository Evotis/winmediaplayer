﻿
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;

namespace WindowsMediaPlayer
{
    public partial class MainWindow
    {
        System.Windows.Controls.Button BArtist;
        System.Windows.Controls.Button BGenre;
        System.Windows.Controls.Button BAlbum;
        static public Dictionary<string, string> idGenre;

        public System.Windows.Controls.ListBox initializeListBox()
        {
            System.Windows.Controls.ListBox tmp;

            tmp = new System.Windows.Controls.ListBox
            {
                Name = "Box",
                Height = 433,
                Width = 500,
                Background = Brushes.Black,
                Foreground = Brushes.White
            };
            return tmp;
        }

        public System.Windows.Controls.Button initializeBArtist()
        {
            System.Windows.Controls.Button tmp;

            tmp = new System.Windows.Controls.Button
            {
                Name = "BArtist",
                Height = 30,
                Width = 156,
                Background = Brushes.Black,
                Foreground = Brushes.White,
                Margin = new Thickness(0,
                            0,
                            0,
                            0),
                HorizontalAlignment = HorizontalAlignment.Right,
                VerticalAlignment = VerticalAlignment.Bottom,
                Content = "Sort by artists"
            };
            tmp.Click += SortByArtist;
            return tmp;
        }

        public System.Windows.Controls.Button initializeBGenre()
        {
            System.Windows.Controls.Button tmp;


            tmp = new System.Windows.Controls.Button
            {
                Name = "BGenre",
                Height = 30,
                Width = 165,
                Background = Brushes.Black,
                Foreground = Brushes.White,
                Margin = new Thickness(0,
                            -40,
                            155,
                            0),
                HorizontalAlignment = HorizontalAlignment.Right,
                VerticalAlignment = VerticalAlignment.Bottom,
                Content = "Sort by genre"
            };
            tmp.Click += SortByGenre;
            return tmp;
        }

        public System.Windows.Controls.Button initializeBAlbum()
        {
            System.Windows.Controls.Button tmp;


            tmp = new System.Windows.Controls.Button
            {
                Name = "BAlbum",
                Height = 30,
                Width = 165,
                Background = Brushes.Black,
                Foreground = Brushes.White,
                Margin = new Thickness(1,
                            -40,
                            0,
                            0),
                HorizontalAlignment = HorizontalAlignment.Left,
                VerticalAlignment = VerticalAlignment.Bottom,
                Content = "Sort by album"
            };
            tmp.Click += SortByAlbum;
            return tmp;
        }

        private void initIdGenre()
        {
            idGenre.Add("(0)", "Blues");
            idGenre.Add("(1)", "Classic Rock");
            idGenre.Add("(2)", "Country");
            idGenre.Add("(3)", "Dance");
            idGenre.Add("(4)", "Disco");
            idGenre.Add("(5)", "Funk");
            idGenre.Add("(6)", "Grunge");
            idGenre.Add("(7)", "Hip-Hop");
            idGenre.Add("(8)", "Jazz");
            idGenre.Add("(9)", "Metal");
            idGenre.Add("(10)", "New Age");
            idGenre.Add("(11)", "Oldies");
            idGenre.Add("(12)", "Other");
            idGenre.Add("(13)", "Pop");
            idGenre.Add("(14)", "R&B");
            idGenre.Add("(15)", "Rap");
            idGenre.Add("(16)", "Reggae");
            idGenre.Add("(17)", "Rock");
            idGenre.Add("(18)", "Techno");
            idGenre.Add("(19)", "Industrial");
            idGenre.Add("(20)", "Alternative");
            idGenre.Add("(21)", "Ska");
            idGenre.Add("(22)", "Death Metal");
            idGenre.Add("(23)", "Pranks");
            idGenre.Add("(24)", "Soundtrack");
            idGenre.Add("(25)", "Euro-Techno");
            idGenre.Add("(26)", "Ambient");
            idGenre.Add("(27)", "Trip-Hop");
            idGenre.Add("(28)", "Vocal");
            idGenre.Add("(29)", "Jazz+Funk");
            idGenre.Add("(30)", "Jazz+Funk");
            idGenre.Add("(31)", "Trance");
            idGenre.Add("(32)", "Classical");
            idGenre.Add("(33)", "Instrumental");
            idGenre.Add("(34)", "Acid");
            idGenre.Add("(35)", "House");
            idGenre.Add("(36)", "Game");
            idGenre.Add("(37)", "Sound Clip");
            idGenre.Add("(38)", "Gospel");
            idGenre.Add("(39)", "Noise");
            idGenre.Add("(40)", "AlternRock");
            idGenre.Add("(41)", "Bass");
            idGenre.Add("(42)", "Soul");
            idGenre.Add("(43)", "Punk");
            idGenre.Add("(44)", "Space");
            idGenre.Add("(45)", "Meditative");
            idGenre.Add("(46)", "Instrumental Pop");
            idGenre.Add("(47)", "Instrumental Rock");
            idGenre.Add("(48)", "Ethnic");
            idGenre.Add("(49)", "Gothic");
            idGenre.Add("(50)", "Darkwave");
            idGenre.Add("(51)", "Techno-Industrial");
            idGenre.Add("(52)", "Electronic");
            idGenre.Add("(53)", "Pop-Folk");
            idGenre.Add("(54)", "Eurodance");
            idGenre.Add("(55)", "Dream");
            idGenre.Add("(56)", "Southern Rock");
            idGenre.Add("(57)", "Comedy");
            idGenre.Add("(58)", "Cult");
            idGenre.Add("(59)", "Gangsta");
            idGenre.Add("(60)", "Top 40");
            idGenre.Add("(61)", "Christian Rap");
            idGenre.Add("(62)", "Pop/Funk");
            idGenre.Add("(63)", "Jungle");
            idGenre.Add("(64)", "Native American");
            idGenre.Add("(65)", "Cabaret");
            idGenre.Add("(66)", "New Wave");
            idGenre.Add("(67)", "Psychadelic");
            idGenre.Add("(68)", "Rave");
            idGenre.Add("(69)", "Showtunes");
            idGenre.Add("(70)", "Trailer");
            idGenre.Add("(71)", "Lo-Fi");
            idGenre.Add("(72)", "Tribal");
            idGenre.Add("(73)", "Acid Punk");
            idGenre.Add("(74)", "Acid Jazz");
            idGenre.Add("(75)", "Polka");
            idGenre.Add("(76)", "Retro");
            idGenre.Add("(77)", "Musical");
            idGenre.Add("(78)", "Rock & Roll");
            idGenre.Add("(79)", "Hard Rock");
            idGenre.Add("(80)", "Anonymous");
        }
    }
}
