﻿using Id3;
using System.IO;

namespace WindowsMediaPlayer
{
    public class Playlist
    {
        public string FileName { get; set; }
        public string SafeFileName { get; set; }
        public string Album { get; set; }
        public string Artist { get; set; }
        public string Genre { get; set; }

        public void fillPlaylist(string OfdFileName, string OfdSafeFileName, int count)
        {
            Id3Tag tag;
            Mp3File mp3;

            if (Path.GetExtension(OfdFileName) == ".mp3")
            {
                if ((mp3 = new Mp3File(OfdFileName)) != null)
                {
                    if ((tag = mp3.GetTag(Id3TagFamily.FileStartTag)) != null)
                    {
                        Album = tag.Album;
                        Artist = tag.Artists;
                        Genre = tag.Genre;
                    }
                }
            }
            if (Artist == null)
                Artist = "Anonymous";
            if (Album == null)
                Album = "Anonymous";
            if (Genre == null || !MainWindow.idGenre.ContainsKey(Genre))
                Genre = "(80)";
            FileName = OfdFileName;
            SafeFileName = OfdSafeFileName;
        }

        public override string ToString()
        {
            switch (MainWindow._display)
            {
                case MainWindow.Display.Artist:
                    return Artist + " - " + SafeFileName;
                case MainWindow.Display.Album:
                    return Album + " - " + SafeFileName;
                default:
                    return MainWindow.idGenre[Genre] + " - " + SafeFileName;
            }
        }
    }
}
