﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Forms;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Application = System.Windows.Application;
using MessageBox = System.Windows.MessageBox;

namespace WindowsMediaPlayer
{
    public partial class MainWindow
    {
        public enum Display
        {
            Artist,
            Album,
            Genre
        };

        System.Windows.Controls.ListBox Box;
        public ObservableCollection<Playlist> QueueList;
        static public Display _display = Display.Artist;
        private readonly string[] PlayExtention = { ".mp3", ".wmv", ".avi", ".mp4", "" };
        private bool bugged = false;

        private void add_playlist(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            Playlist pl;
            int count = 0;

            ofd.Multiselect = true;
            ofd.ShowDialog();
            if (Array.IndexOf(PlayExtention, Path.GetExtension(ofd.FileName)) == -1)
            {
                MessageBox.Show("Cannot display this file format");
                return;
            }
            Video.MediaOpened += Video_MediaOpened;
            foreach (string FileName in ofd.FileNames)
            {
                try
                {
                    if (FileName != "")
                    {
                        if (Array.IndexOf(PlayExtention, Path.GetExtension(FileName)) != -1)
                        {
                            pl = new Playlist();
                            pl.fillPlaylist(FileName, ofd.SafeFileNames[count], QueueList.Count);
                            QueueList.Add(pl);
                        }
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("File " + FileName + " corrupted.");
                }
                count += 1;
            }
        }
        
        private void Double_Click(object sender, MouseButtonEventArgs e)
        {
            if (Box.SelectedItem == null)
                return;
            if (((Playlist)Box.SelectedItem).FileName != "")
            {
                Video.Source = new Uri(((Playlist)Box.SelectedItem).FileName);
                timeElapsed.Minimum = 0;
                timeElapsed.Value = 0;
                Video.Play();
                if (timer != null)
                    timer.Stop();
                if (Video.NaturalDuration.HasTimeSpan)
                    timeElapsed.Maximum = Video.NaturalDuration.TimeSpan.TotalSeconds;
                timer = new System.Timers.Timer { Interval = TimeSpan.FromSeconds(1).TotalMilliseconds };
                timer.Elapsed += timer_Elapsed;
                timer.Start();
                PlayIndex = Box.SelectedIndex;
                Play.Content = "Pause";
                if (!isHidden)
                {
                    Play.Visibility = Visibility.Collapsed;
                    Pause.Visibility = Visibility.Visible;
                }
                isPlaying = true;
                isChanging = false;
            }
        }

        private void Playlist_OnPreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
                if (Box.SelectedIndex != -1)
                    QueueList.RemoveAt(Box.SelectedIndex);
        }

        private void ChangeVideo()
        {
            if (QueueList.Count > PlayIndex && QueueList[PlayIndex].FileName != "")
            {
                if (QueueList.Count > PlayIndex + 1)
                    PlayIndex += 1;
                else
                    PlayIndex = 0;
                Video.Source = new Uri(QueueList[PlayIndex].FileName);
                timeElapsed.Minimum = 0;
                timeElapsed.Value = 0;
                Video.Play();
                if (timer != null)
                    timer.Stop();
                if (Video.NaturalDuration.HasTimeSpan)
                    timeElapsed.Maximum = Video.NaturalDuration.TimeSpan.TotalSeconds;
                timer = new System.Timers.Timer { Interval = TimeSpan.FromSeconds(1).TotalMilliseconds };
                timer.Elapsed += timer_Elapsed;
                timer.Start();
                Play.Content = "Pause";
                isPlaying = true;
                isChanging = false;
                if (Video.NaturalDuration.HasTimeSpan && timeElapsed.Maximum == 10)
                    timeElapsed.Maximum = Video.NaturalDuration.TimeSpan.TotalSeconds;
            }
        }

        private void SavePlaylist()
        {
            XmlSerializer xs = new XmlSerializer(typeof(ObservableCollection<Playlist>));

            using (StreamWriter wr = new StreamWriter("Playlist.xml"))
            {
                xs.Serialize(wr, QueueList);
            }
        }

        private void LoadPlaylist()
        {
            QueueList = new ObservableCollection<Playlist>();

            XmlSerializer xs = new XmlSerializer(typeof(ObservableCollection<Playlist>));
            using (StreamReader rd = new StreamReader("Playlist.xml"))
            {
                QueueList = xs.Deserialize(rd) as ObservableCollection<Playlist>;
            }
            if (QueueList != null)
            {
                foreach (Playlist pl in QueueList.Where(pl => !File.Exists(pl.FileName)))
                    bugged = true;
                if (bugged)
                    QueueList = new ObservableCollection<Playlist>();
            }
            Box.ItemsSource = QueueList;
        }
    }
}
