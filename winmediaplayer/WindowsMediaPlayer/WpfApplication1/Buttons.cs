﻿using System;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using MessageBox = System.Windows.MessageBox;
using Timer = System.Timers.Timer;

namespace WindowsMediaPlayer
{
    public partial class MainWindow
    {
        bool isPlaying = true;
        bool PlaylistVisible;

        void play_video(object sender, EventArgs e)
        {
            if (!isPlaying && Video.Source != null)
            {
                isPlaying = true;
                Play.Content = "Pause";
                Video.Play();
                Play.Visibility = Visibility.Collapsed;
                Pause.Visibility = Visibility.Visible;
            }
            else if (isPlaying && Video.Source != null)
            {
                Video.Pause();
                Play.Content = "Play";
                isPlaying = false;
                Play.Visibility = Visibility.Visible;
                Pause.Visibility = Visibility.Collapsed;
            }
        }

        private void FullScreen(object sender, MouseButtonEventArgs e)
        {
            if ((doubleClick >= 2 || e == null) && fullScreen == false)
            {
                WindowStyle = WindowStyle.None;
                WindowState = WindowState.Maximized;
                fullScreen = !fullScreen;
                doubleClick = 0;
                DoubleCount = 0;
            }
            else if ((doubleClick >= 2 || e == null) && fullScreen)
            {
                WindowStyle = WindowStyle.SingleBorderWindow;
                WindowState = WindowState.Normal;
                fullScreen = !fullScreen;
                doubleClick = 0;
                DoubleCount = 0;
            }
            doubleClick += 1;
        }

        public void open_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();

            if (isPlaying)
            {
                Video.Stop();
                Play.Content = "Play";
                isPlaying = false;
            }
            ofd.AddExtension = true;
            ofd.ShowDialog();
            Video.MediaOpened += Video_MediaOpened;
            if (Array.IndexOf(extention, Path.GetExtension(ofd.FileName)) == -1)
            {
                MessageBox.Show("Cannot display this file format");
                return;
            }
            if (ofd.FileName != "")
            {
                Video.Source = new Uri(ofd.FileName);
                timeElapsed.Minimum = 0;
                timeElapsed.Value = 0;
                isPlaying = true;
                Video.Play();
                Play.Content = "Pause";
                Play.Visibility = Visibility.Collapsed;
                if (timer != null)
                    timer.Stop();
                timer = new Timer {Interval = TimeSpan.FromSeconds(1).TotalMilliseconds};
                timer.Elapsed += timer_Elapsed;
                timer.Start();
                PlayIndex = 0;
                isChanging = false;
            }
        }

        private void list_Click(object sender, RoutedEventArgs e)
        {
            if (PlaylistVisible == false)
                Playlist.Show();
            else
                Playlist.Hide();
            PlaylistVisible = !PlaylistVisible;
        }

        private void try_closing(object sender, CancelEventArgs e)
        {
            Plist.Content = "Show Playlist";
            Playlist.Hide();
            PlaylistVisible = !PlaylistVisible;
            e.Cancel = true;
        }

        private void next_video(object sender, RoutedEventArgs e)
        {
            ChangeVideo();
            if (!isHidden)
            {
                Play.Visibility = Visibility.Collapsed;
                Pause.Visibility = Visibility.Visible;
            }
        }

        private void previous_video(object sender, RoutedEventArgs e)
        {
            if (QueueList.Count > PlayIndex && QueueList[PlayIndex].FileName != "")
            {
                if (PlayIndex - 1 >= 0)
                    PlayIndex -= 1;
                else
                    PlayIndex = QueueList.Count - 1;
                Video.Source = new Uri(QueueList[PlayIndex].FileName);
                timeElapsed.Minimum = 0;
                timeElapsed.Value = 0;
                Video.Play();
                if (timer != null)
                    timer.Stop();
                if (Video.NaturalDuration.HasTimeSpan)
                    timeElapsed.Maximum = Video.NaturalDuration.TimeSpan.TotalSeconds;
                timer = new Timer { Interval = TimeSpan.FromSeconds(1).TotalMilliseconds };
                timer.Elapsed += timer_Elapsed;
                timer.Start();
                Play.Content = "Pause";
                isPlaying = true;
                isChanging = false;
                if (Video.NaturalDuration.HasTimeSpan && timeElapsed.Maximum == 10)
                    timeElapsed.Maximum = Video.NaturalDuration.TimeSpan.TotalSeconds;
                if (!isHidden)
                {
                    Play.Visibility = Visibility.Collapsed;
                    Pause.Visibility = Visibility.Visible;
                }
            }
        }
    }
}